package com.example.biblioteka.Book;

import com.example.biblioteka.Book.model.Book;
import com.example.biblioteka.Book.repository.BookRepository;
import com.example.biblioteka.Book.service.BookService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;
import java.util.Random;

import static org.mockito.ArgumentMatchers.any;


@RunWith(SpringRunner.class)
@SpringBootTest
public class BookTests {

    private static final String BOOK_TITLE = "title";
    private static final String BOOK_AUTHOR = "author";
    private static final String BOOK_ISBN = "isbn";

    @Autowired
    private BookService bookService;

    @MockBean
    private BookRepository bookRepository;

    @Before
    public void setUp() {
        Book book = new Book(BOOK_TITLE,BOOK_AUTHOR,BOOK_ISBN);
        Mockito.when(bookRepository.findById(any(Long.class))).thenReturn(java.util.Optional.ofNullable(book));
        Mockito.when(bookRepository.countBooksByDOBIsNull()).thenReturn(2);
    }

    @Test
    @DisplayName("Should find book for given id")
    public void shouldFindBook() {
        Long id = new Random().nextLong();
        Optional<Book> book = bookService.findById(id);
        Assert.assertEquals(BOOK_TITLE, book.get().getTitle());
        Assert.assertEquals(BOOK_AUTHOR, book.get().getAuthor());
        Assert.assertEquals(BOOK_ISBN, book.get().getISBN());
    }
    @Test
    @DisplayName("Should find books to borrow")
    public void shouldFindFreeBooks(){
        Assert.assertEquals(bookService.countFreeBooks(),2);
        }

}
