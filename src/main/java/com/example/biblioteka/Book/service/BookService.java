package com.example.biblioteka.Book.service;

import com.example.biblioteka.Book.model.Book;
import com.example.biblioteka.Book.repository.BookRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BookService {

    private BookRepository bookRepository;

    public BookService(BookRepository bookRepository){
        this.bookRepository = bookRepository;
    }

    public Book saveBook(Book book){
        return bookRepository.save(book);
    }
    public Optional<Book> findById(Long id){return  bookRepository.findById(id);}
    public void deleteBookById(Long id){
        bookRepository.deleteById(id);
    }
    public Long countBooks(){
       return bookRepository.count();
    }

    public int countBorrowedBooks(){
        return bookRepository.countBooksByDOBIsNotNull();
    }

    public int countFreeBooks(){
        return bookRepository.countBooksByDOBIsNull();
    }




}

