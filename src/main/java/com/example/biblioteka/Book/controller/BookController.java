package com.example.biblioteka.Book.controller;

import com.example.biblioteka.Book.model.Book;
import com.example.biblioteka.Book.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class BookController {
    private BookService bookService;
    @Autowired
    public void setBookService(BookService bookService){
        this.bookService = bookService;
    }
    public Book saveBook(Book book){
       return bookService.saveBook(book);
    }
    public void deleteBookById(Long id){
        bookService.deleteBookById(id);
    }
    public Long countBook(){
        return bookService.countBooks();
    }
    public int countBorrowedBooks(){
        return bookService.countBorrowedBooks();
    }
    public int countFreeBooks(){
        return bookService.countFreeBooks();
    }
}
