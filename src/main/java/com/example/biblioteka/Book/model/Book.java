package com.example.biblioteka.Book.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false, insertable = false)
    private Long id;
    @NotNull
    private String title;
    @NotNull
    private String author;
    @NotNull
    private String ISBN;
    private LocalDate dOB;

    public Book(@NotNull String title,@NotNull String author,@NotNull String ISBN){
        this.title = title;
        this.author=author;
        this.ISBN = ISBN;
    }

}
