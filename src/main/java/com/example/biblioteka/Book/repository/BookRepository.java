package com.example.biblioteka.Book.repository;

import com.example.biblioteka.Book.model.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends CrudRepository<Book,Long> {
    int countBooksByDOBIsNull();
    int countBooksByDOBIsNotNull();

}

