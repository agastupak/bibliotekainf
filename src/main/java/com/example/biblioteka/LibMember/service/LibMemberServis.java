package com.example.biblioteka.LibMember.service;

import com.example.biblioteka.Book.model.Book;
import com.example.biblioteka.Book.repository.BookRepository;
import com.example.biblioteka.LibMember.model.LibMember;
import com.example.biblioteka.LibMember.repository.LibMemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.Period;

@Service
public class LibMemberServis {

    private LibMemberRepository libMemberRepository;
    private BookRepository bookRepository;

    public LibMemberServis(LibMemberRepository libMemberRepository, BookRepository bookRepository){
        this.libMemberRepository = libMemberRepository;
        this.bookRepository = bookRepository;
    }

    public LibMember save(LibMember libMember){
        return libMemberRepository.save(libMember);
    }
    public String borrowBook(LibMember libMember,Book book){
        LocalDate localDate = LocalDate.now().plusDays(30);
        book.setDOB(localDate);
        bookRepository.save(book);
        libMember.getBooks().add(book);
        //libMemberRepository.save(libMember);
        return  libMember + " wypozyl ksiazke " + book ;
    }
    public String returnBook(LibMember libMember, Book book, LocalDate localDate){
        if(libMember.getBooks().contains(book)){
            int periodInt=0;
            if(localDate.isAfter(book.getDOB())){
                Period period = Period.between(book.getDOB(),localDate);
                periodInt = period.getDays();
                libMember.setPenalty(libMember.getPenalty()+periodInt);
            }
            libMember.getBooks().remove(book);
            book.setDOB(null);
            return "zwrocono ksiazke " + book + " do zaplaty: " + String.valueOf(periodInt) + " do zaplaty lacznie " + libMember.getPenalty();
        }
        return "nie oddano ksiazki";
    }
}
