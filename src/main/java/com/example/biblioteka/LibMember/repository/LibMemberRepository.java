package com.example.biblioteka.LibMember.repository;

import com.example.biblioteka.Book.model.Book;
import com.example.biblioteka.LibMember.model.LibMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LibMemberRepository extends CrudRepository<LibMember,Long> {
}
