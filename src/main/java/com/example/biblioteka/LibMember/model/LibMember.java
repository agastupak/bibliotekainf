package com.example.biblioteka.LibMember.model;

import com.example.biblioteka.Book.model.Book;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
public class LibMember {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, updatable = false, insertable = false)
    private Long id;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    private int penalty;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<Book> books;

    public LibMember(@NotNull String firstName,@NotNull String lastName){
        this.firstName=firstName;
        this.lastName = lastName;
        this.penalty = 0;
        this.books = new HashSet<>();
    }

}
