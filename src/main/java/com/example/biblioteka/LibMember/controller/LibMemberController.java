package com.example.biblioteka.LibMember.controller;

import com.example.biblioteka.Book.model.Book;
import com.example.biblioteka.LibMember.model.LibMember;
import com.example.biblioteka.LibMember.service.LibMemberServis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.time.LocalDate;

@Controller
public class LibMemberController {

    private LibMemberServis libMemberServis;
    //dodac zaplate kary

    public LibMemberController(LibMemberServis libMemberServis){
        this.libMemberServis = libMemberServis;
    }

    public LibMember save(LibMember libMember){
       return this.libMemberServis.save(libMember);
    }

    public String borrowBook(LibMember libMember, Book book){
        return libMemberServis.borrowBook(libMember,book);
    }

    public String returnBook(LibMember libMember, Book book, LocalDate localDate){
        return libMemberServis.returnBook(libMember,book,localDate);
    }

    public String payPenalty(LibMember libMember,int penalty){
        if(libMember.getPenalty()<=penalty){
            int nPenalty = penalty - libMember.getPenalty();
            libMember.setPenalty(0);
            return "reszta " + nPenalty;
        }else{
            int nPenalty = libMember.getPenalty()-penalty;
            libMember.setPenalty(nPenalty);
            return "zostalo do zaplaty " + nPenalty;
        }

    }
}
