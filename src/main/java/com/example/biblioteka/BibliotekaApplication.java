package com.example.biblioteka;

import com.example.biblioteka.Book.controller.BookController;
import com.example.biblioteka.Book.model.Book;
import com.example.biblioteka.LibMember.controller.LibMemberController;
import com.example.biblioteka.LibMember.model.LibMember;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;
@Slf4j
@SpringBootApplication
public class BibliotekaApplication implements CommandLineRunner {

	private  BookController bookController;
	private  LibMemberController libMemberController;
    @Autowired
    public void setBookController(BookController bookController){
        this.bookController = bookController;
    }
    @Autowired
    public void setLibMemberController(LibMemberController libMemberController){
        this.libMemberController = libMemberController;
    }


	public static void main(String[] args) {
		SpringApplication.run(BibliotekaApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

        LibMember libMember1 = new LibMember("Imie1", "Nazwisko1");
        LibMember libMember2 = new LibMember("Imie2", "Nazwisko2");
        log.info("nowy uzytkownik " + libMemberController.save(libMember1).toString());
        log.info("nowy uzytkownik " + libMemberController.save(libMember2).toString());

        Book book1 = new Book("tytul1", "autor1", "isbn1");
        Book book2 = new Book("tytul2", "autor2", "isbn2");
        Book book3 = new Book("tytul3", "autor3", "isbn3");
        Book book4 = new Book("tytul4", "autor4", "isbn4");
        Book book5 = new Book("tytul5", "autor5", "isbn5");
        Book book6 = new Book("tytul6", "autor6", "isbn6");

        log.info(bookController.saveBook(book1).toString());
        log.info(bookController.saveBook(book2).toString());
        bookController.saveBook(book3);
        bookController.saveBook(book4);
        bookController.saveBook(book5);
        bookController.saveBook(book6);

        log.info(libMemberController.borrowBook(libMember1,book1));
        log.info(libMemberController.borrowBook(libMember2,book2));
        log.info(libMemberController.borrowBook(libMember2,book3));

        log.info("ilosc ksiazek " + bookController.countBook().toString());
        log.info("ilosc ksiazek wypozyczone "+ bookController.countBorrowedBooks());
        log.info("ilosc ksiazek do wypozyczenia " + bookController.countFreeBooks());

        LocalDate localDate = LocalDate.now().plusDays(40);

        log.info(libMemberController.returnBook(libMember1,book1,localDate));
        log.info(libMemberController.returnBook(libMember2,book2,localDate));
        log.info(libMemberController.returnBook(libMember2,book3,localDate));

        log.info(libMemberController.payPenalty(libMember2,5));
	}
}
